use std::ptr;

struct AlignedBuffer {
    buf: Vec<u8>, 
    ptr: *const u8,
}

fn is_ptr_aligned(ptr: *const u8, alignment: usize) -> bool { 
    0 == ((ptr as usize) & (alignment - 1)) 
}

impl AlignedBuffer {
    pub fn alloc(size: usize, alignment: usize) -> AlignedBuffer {
        assert!(alignment.is_power_of_two());
        let mut ab = AlignedBuffer { buf: Vec::new(), ptr: ptr::null() };
        ab.buf.resize(size + alignment, 0);
        let mut align_offset = 0; 
        while !is_ptr_aligned(&ab.buf[align_offset] as *const u8, alignment) {
            align_offset += 1
        } 
        ab.ptr = (&ab.buf[align_offset] as *const u8);
        ab
    }
    pub fn aligned_ptr(&self) -> *const u8 { 
        self.ptr
    }
}

/*
fn get_aligned<T>(align: usize) -> (Vec<u8>, T) {
    let t: T = {};
    let buf = vec![0; std::mem::size_of(t) + align];
    (buf, t)
}*/


#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn buf0() {
        let ab0 = AlignedBuffer::alloc(2048, 64);
        println!("aligned buffer allocated");
        println!("base buf at  {:?}", &ab0.buf[0] as *const u8);
        println!("align ptr at {:?}", ab0.aligned_ptr()) ;
        let ab1 = AlignedBuffer::alloc(8192, 4096);
        println!("aligned buffer allocated");
        println!("base buf at  {:?}", &ab1.buf[0] as *const u8);
        println!("align ptr at {:?}", ab1.aligned_ptr()) ;
        let ab2 = AlignedBuffer::alloc(2usize.pow(15), 2usize.pow(13));
        println!("aligned buffer allocated");
        println!("base buf at  {:?}", &ab2.buf[0] as *const u8);
        println!("align ptr at {:?}", ab2.aligned_ptr()) ;
    }

}

